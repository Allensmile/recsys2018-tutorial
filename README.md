# RecSys 2018 tutorial *Emotions and Personality in Recommender Systems* 

## Introduction

This repository contains the slides and the code for the [RecSys 2019 tutorial](https://recsys.acm.org/recsys18/tutorials/#content-tab-1-2-tab)

**Emotions and Personality in Recommender Systems**

by *Marko Tkalčič* (Free University of Bozen-Bolzano, Italy)

Psychological aspects of item consumption have been under-explored in the RecSys community. For example, a movie usually contains a roller-coaster of emotions, but the user preference for such a complex experience in recommender systems is usually expressed as one numerical score. Hence, a lot of information is hidden in the psychological characteristics of the user, the emotional characteristics of the item and in the context where the interaction occurs.

This tutorial will consist of 2 parts:

- a lecture, providing a good-enough understanding of the psychological background and
- a hands-on session showing how to build an interface for preference elicitation with emotions and personality acquisition.

The tutorial targets early-stage researchers in recommender systems. They should have a basic understanding of recommender systems. For the hands-on part, they should have a basic command of HTML and JavaScript.

Date: Tuesday, Oct 2, 2018, 16:00-17:30

Location: Parq A 


## Pre-requisites

In order to be able to implement the hands-on part you should have familiarity with programming. The tutorial will be on web client-side (HTML/DOM/Javascript), although you may use another solution (Android, iOS/macOS, Linux, Unity, Windows).


Before the session starts please make sure that you:

- have a laptop with an integrated camera
- have the Chrome browser installed
- (optionally) installed an HTTP server on your laptop
	- apache
	- MAMP/XAMPP or some other integrated solution
	- know where your ```htdocs``` folder is

## References

Tkalčič, M. (2018). Emotions and personality in recommender systems. In Proceedings of the 12th ACM Conference on Recommender Systems - RecSys ’18 (Vol. 38, pp. 535–536). New York, New York, USA: ACM Press. https://doi.org/10.1145/3240323.3241619 

